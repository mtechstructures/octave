% This script is for design of slab
ly = 8000; % length of longer span of slab in mm
lx = 3000; % length of shorter span of slab in mm
%type of slab
TS = (ly/lx); disp('This is one way slab')
% Trial depth of slab
M.F = 1.5; % Magnification Factor
leff = 3230; % Effective Length in mm
d1 = (leff/(20*M.F)); disp (['the trial depth of slab = ', num2str(d1)])
% Assuming 15 mm clear cover and 8 mm bar
D = d1 + 15 + (8/2); % in mm
disp(['Overall Depth D =  ',num2str(D) '~= 130'])
d = 130 -15 - (8/2); % Depth of slab in mm
disp(['Depth of slab d =',num2str(d)])
% Load Calculation
DL = 1*0.13*25; % Dead Load in KN/m^2
LL = 2.5; % Live Load in KN/m^2
FL = 0.5; % Floor Load in KN/m^2
W = DL + LL + FL; % Total Load in KN/m
Wu = 1.5*W; % Ultimate Load in KN/m
disp([' Ultimate Load Wu = ',num2str(Wu)])
Leff = lx + d; % Effective Span in mm
disp(['Effective Span Leff = ',num2str(Leff)])
Mu = (((Wu*Leff^2)/8)/10^6); % Max Bending Moment in KNm
disp(['Max Bending Mu = ',num2str(Mu)])
% Check for depth
fck = 20; % characteristic strength in N/mm^2
b = 1000; % width in mm
d2 = sqrt((Mu*10^6)/(0.136*fck*b));
disp(['Depth required = ',num2str(d2) ' < ',num2str(d) ' ,' 'Hence Safe'])
% Design of Main Steel
fy = 415; % yield strength of steel
Astreq = ((0.5*fck)/fy)*(1 - sqrt(1 -((4.6*Mu*10^6)/(fck*b*d^2))))*(b*d);
disp(['Area of main steel Ast = ',num2str(Astreq)])
Astmin = ((0.12/100)*b*D); disp(['Astmin = ',num2str(Astmin)])
Fi = 8; % Diameter of steel in mm
Smax = ((1000*(pi/4)*Fi^2)/Astreq); disp(['Smax = ',num2str(Smax)])
S = 300; % Min spacing to be provided in mm
disp(['But min spacing provided = ' ,num2str(S) ' > Smax ,Hence Safe'])
% Distribution Reinforcement
ASTmin = ((0.15/100)*b*D); disp(['ASTmin = ',num2str(ASTmin)])
% Spacing for distribution
fi = 6; % Diameter of distribution steel
smax = ((1000*(pi/4)*fi^2)/ASTmin); disp(['smax = ',num2str(smax)])
s = 450; % Min spacing to be provided for distribution steel
disp(['But min spacing provided = ' ,num2str(s) ' > smax ,Hence Safe'])
% Check for Shear Stress
Vu = ((Wu*Leff)/2); % Ultimate Shear Force
disp(['Ultimate Shear Force Vu =',num2str(Vu)])
TAUv = (Vu*10^3)/(b*d); % Shear stress in N/mm^2
disp(['TAUv = ',num2str(TAUv)])
TAUcmin = 0.28; % Critical Shear stress in N/mm^2
disp(['TAUcmin = ',num2str(TAUcmin)])
disp('TAUv < TAUcmin , Hence Safe') 
% Check for Deflection
Astprov = ((1000*(pi/4)*Fi^2)/Smax); % Area of steel tp be provided
disp(['Astprov = ',num2str(Astprov)])
fs = 0.58*fy*(Astreq/Astprov); disp(['fs = ',num2str(fs)])
Pt = 100*(Astprov/(b*d)); % Percentage of steel
disp(['Pt = ',num2str(Pt)])
Mf = 1.7; % Magnification factor acc to code
disp(' Therefore Magnification Factor Mf = 1.7')
actual = (Leff/d); disp(['(L/d)actual = ',num2str(actual)])
Mf = 1.7; % Magnification factor acc to code
permissible = 20*Mf; disp(['(L/d)permissible = ',num2str(permissible)])
disp('(L/d)actual <= (L/d)permissible , Hence Safe')


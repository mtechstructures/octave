% FOUNDATION DESIGN OF COMBINED FOOTING
%C1 is left column & P1 is lOAD ON C1 and C2 is right side column & P2 is load on C2
P1 = 900; %KN
P2 = 1600; % KN
S = 4.0; % Distance Between two columns is S in m
Df = 1.5; % Df is depth of foundation in m
% size of columns
b1 = 400; % in mm
d1 = 400; % in mm
b2 = 500; % in mm
d2 = 500; % in mm
BC = 150 % Bearing Capacity of soil at 1.5m depth in KN/m^2 
fy = 415; % characteristic strength of streel
fck = 20; % characteristic strength of concrete 
Cover = 50; % in mm
% Areq is the area required for foundation 
Areq = [((P1+P2)*1.15)/BC];
X = [(P2*S)/(P1+P2)]; % x is the distance of c.g. of footing from centre of C1
L = 6.0 % in m
B = (Areq/L)+ 0.3 % increase by 0.3m to safe in base pressure check
Aprov = B*L
LeftProjection = [(L/2)-X] 
RightProjection = [(L/2)-(S-X)] 
p = [(P1+P2)/L] % Upward pressure in Long. Direction in KN/m
disp('CHECK FOR ONE WAY SHEAR');
Maxspan = max(LeftProjection,RightProjection)
shearatcolumn1 = [(p*LeftProjection)-P1]*(-1)
shearatcolumn2 = [shearatcolumn1-(p*S)]*(-1)
x = [(shearatcolumn1*S)/(shearatcolumn1+shearatcolumn2)]*1000; % Distance of zero shear from C1
x1 =[(S*1000)-x];  % Distance of zero shear from C2
%Assume Pt = 0.5 and grade of concrete = M20
pt = 0.50
tauc = 0.48; % From IS :456-2000 
if LeftProjection < RightProjection
  d = [((x1-(b2/2))*shearatcolumn2)/x1]/[(tauc*B)+(shearatcolumn2/x1)]% in mm 
else
   d = [((x-(b1/2))*shearatcolumn1)/x]/[(tauc*B)+(shearatcolumn1/x)]% in mm 
end
disp('CHECK FOR TWO WAY SHEAR');
d = 460 % Approximated as above calculated
shortside= min(b1,d1)
longside = max(b1,d1)
beta = shortside\longside;
Kscal= 0.5 + beta;
if Kscal > 1
  Ks = 1;
else
  Ks = Kscal;
end
tauc2 = Ks*0.25*sqrt(fck); % Limiting two way shear stress
Perimeter = (b1 + d1)*2;
Vu1 = P1-[((P1+P2)/(L*B))*(((d/1000)+(b1/1000))*((d/1000)+(d1/1000)))] % Punching shear force in KN
Vuc1 = (Perimeter*tauc2*d)/1000
if Vuc1 > Vu1
  disp('Hence safe and depth of footing is govern by one way shear');
else
  disp('Column 1 Fail in punching Shear');
end
shortside= min(b2,d2)
longside = max(b2,d2)
beta = shortside\longside;
Kscal= 0.5 + beta;
if Kscal > 1
  Ks = 1;
else
  Ks = Kscal;
end
tauc2 = Ks*0.25*sqrt(fck); % Limiting two way shear stress
Perimeter = (b2 + d2)*2;
Vu2 = P1-[((P1+P2)/(L*B))*(((d/1000)+(b2/1000))*((d/1000)+(d2/1000)))] %  Punching shear force in KN
Vuc2 = (Perimeter*tauc2*d)/1000
if Vuc2 > Vu2
  disp('Hence safe and depth of footing is govern by one way shear');
else
  disp('Column 2 Fail in punching Shear');
end
disp('CHECK FOR BASE PRESSURE');
%D = d + (cover = 50) + (dia of long. bar = 16)
BarDia = 20 % For longitudnal reinforcement provided at top
D = d+Cover+BarDia % in mm
q = [(P1+P2)/(L*B)] + (24*(D/1000)) + (Df-(D/1000))*18
if q > BC
  disp('NOT SAFE..');
  disp('Increase Area of footing');
else
  disp('SAFE');
  disp('DESIGN OF LONGITUDNAL FLEXURAL REINFORCEMENT');
%Negative Bending Moment. The steel in this provided at top
Mu1 = [p*(LeftProjection)*((LeftProjection/2)+(x/1000))+(p*(x/1000)*((x/1000)/2))-(P1*(x/1000))]*(-1) % in KNm
ptbending = [[1-[sqrt(1-((4.6*Mu1*1000000)/(fck*B*1000*d*d)))]]*(fck/(2*fy))]*100
Astreq = (pt*B*1000*d)/100 % in mm^2
Astmin = 0.0012*B*1000*D % in mm^2
AreaOneBar = (3.14/4)*(BarDia)^2 % in mm^2
NoOfBars = Astreq/AreaOneBar
Spacing = [(B*1000)-(Cover*2)-BarDia]/(NoOfBars-1)
%Positive Bending Monment
if RightProjection>  LeftProjection
Mu2 = p*((RightProjection-(b2/1000))^2)/2 % in KNm
else
Mu2 = p*((LeftProjection-(b1/1000))^2)/2 % in KNm
end
ptbending = [[1-[sqrt(1-((4.6*Mu2*1000000)/(fck*B*1000*d*d)))]]*(fck/(2*fy))]*100
Astreq = (ptbending*B*1000*d)/100 % in mm^2
Astmin = 0.0012*B*1000*D % in mm^2
BarDia = 16  % For longitudnal reinforcement provided at bottom
AreaOneBar = (3.14/4)*(BarDia)^2 % in mm^2
NoOfBars = max(Astreq,Astmin)/AreaOneBar
Spacing = [(B*1000)-(Cover*2)-BarDia]/(NoOfBars-1)
%Design of column strips as trnsverse beams
disp('Design of column strips as trnsverse beams');
disp('Under Column C1');
p = P1/B % in KN/m
Projection = ((B*1000)-d1)/2 % in mm
Mu1 = (p*(Projection/1000)^2)/2 % in KNm
DiaOfBar = 12  % in mm
d = D-Cover-BarDia-(DiaOfBar/2) % Effective Depth in Transverse Direction
WidthOfBeam = b1+(0.75*d)+(0.75*d)  % in mm
pt = [[1-[sqrt(1-((4.6*Mu1*1000000)/(fck*WidthOfBeam*1000*d*d)))]]*(fck/(2*fy))]*100
Astreq = (pt*WidthOfBeam*d)/100 % in mm^2
Astmin = 0.0012*WidthOfBeam*D % in mm^2
AreaOneBar = (3.14/4)*(DiaOfBar)^2 % in mm^2
NoOfBars = max(Astreq,Astmin)/AreaOneBar
disp('Under Column C2');
p = P2/B % in KN/m
Projection = ((B*1000)-d2)/2 % in mm
Mu2 = (p*(Projection/1000)^2)/2 % in KNm
DiaOfBar = 12  % in mm
d = D-Cover-BarDia-(DiaOfBar/2) % Effective Depth in Transverse Direction
WidthOfBeam = b2+(0.75*d)+(0.75*d) % in mm
pt = [[1-[sqrt(1-((4.6*Mu2*1000000)/(fck*WidthOfBeam*1000*d*d)))]]*(fck/(2*fy))]*100
Astreq = (pt*WidthOfBeam*d)/100 % in mm^2
Astmin = 0.0012*WidthOfBeam*D % in mm^2
AreaOneBar = (3.14/4)*(DiaOfBar)^2 % in mm^2
NoOfBars = max(Astreq,Astmin)/AreaOneBar
end
